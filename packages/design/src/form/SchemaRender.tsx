import * as React from 'react'

export type Schema = {
  type: 'object' | 'array';
  properties: Record<string, Schema>
}

export default function SchemaRenderer(props: Schema) {

}