import * as React from 'react'
import { useForm, Controller, useFieldArray } from 'react-hook-form'
import { Input, Checkbox, Select, Button, Form, FormItemProps } from 'antd'

const Field = ({
  children,
  ...rest
}: React.PropsWithChildren<FormItemProps>) => (
  <Form.Item
    labelCol={{
      span: 3
    }}
    wrapperCol={{
      span: 18
    }}
    {...rest}
  >
    {children}
  </Form.Item>
)

export default function ReactHookForm() {
  const { control, handleSubmit } = useForm({
    defaultValues: {
      rates: [
        { term: 12, minRate: '', maxRate: '', checked: false },
        { term: 18, minRate: '', maxRate: '', checked: false }
      ],
      addr: '',
      agree: false,
      gender: undefined,
    }
  })
  const { fields } = useFieldArray({
    name: 'rates',
    control
  })
  const onSubmit = (data: unknown) => {
    console.log(JSON.stringify(data, null, 2))
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field label="地址">
        <Controller
          name="addr"
          control={control}
          render={({ field }) => <Input {...field} />}
        />
      </Field>
      <Field label="dd">
        <Controller
          name="agree"
          control={control}
          render={
            ({ field: { value, onChange } }) => (
              <Checkbox onChange={onChange} checked={value}>同意</Checkbox>
            )
          }
        />
      </Field>
      <Field label="gender">
        <Controller
          name="gender"
          control={control}
          render={
            ({ field }) => (
              <Select {...field} placeholder="gender">
                <Select.Option value="male">male</Select.Option>
                <Select.Option value="female">female</Select.Option>
              </Select>
            )
          }
        />
      </Field>
      <Field label="利率">
        {fields.map((data, index) => (
          <>
            <Controller
              name={`rates.${index}.checked`}
              control={control}
              render={
                ({ field: { value, onChange } }) => (
                  <Checkbox onChange={onChange} checked={value}>{data.term}期</Checkbox>
                )
              }
            />
            <Controller
              name={`rates.${index}.minRate`}
              control={control}
              render={({ field }) => <Input {...field} placeholder="利率下限"/>}
            />
            <Controller
              control={control}
              name={`rates.${index}.maxRate`}
              render={({ field }) => <Input {...field} placeholder="利率上限"/>}
            />
          </>
        ))}
      </Field>
      <Field label=" " colon={false}>
        <Button type="primary" htmlType="submit">submit</Button>
      </Field>
    </form>
  )
}