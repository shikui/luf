import * as React from 'react'
import { Form, Input, Button, Checkbox, Select } from 'antd'

const { Item, useForm } = Form

export default function AntdForm() {
  const [form] = useForm()
  const handleSubmit = () => {
    console.log(form.getFieldsValue())
  }
  return (
    <Form
      form={form}
      onFieldsChange={(_, allFields) => {
        // console.log(_, allFields)
      }}
    >
      <Item name="carType" label="金融类型">
        <Select placeholder="请选择">
          <Select.Option value={1}>新车</Select.Option>
          <Select.Option value={2}>二手车</Select.Option>
        </Select>
      </Item>
      <Item name="shopName" label="店铺名称">
        <Input placeholder="请输入"/>
      </Item>
      <Item label="利率">
        <Input.Group>
          <Item noStyle name={['rate', 0, 'term']} initialValue={12}><Input type="hidden"/></Item>
          <Item noStyle name={['rate', 0, 'checked']} valuePropName="checked"><Checkbox>12期</Checkbox></Item>
          <Item noStyle name={['rate', 0, 'minRate']}><Input placeholder="最小利率"/></Item>
          <Item noStyle name={['rate', 0, 'maxRate']}><Input placeholder="最大利率"/></Item>
        </Input.Group>
      </Item>
      <Button type="primary" onClick={handleSubmit}>sumbit</Button>
    </Form>
  )
}